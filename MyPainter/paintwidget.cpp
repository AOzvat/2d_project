#include "paintwidget.h"


PaintWidget::PaintWidget(QWidget *parent)
	: QWidget(parent)
{
	setAttribute(Qt::WA_StaticContents);
	modified = false;
	painting = false;
	myPenWidth = 5;
	myPenColor = Qt::blue;
}

bool PaintWidget::openImage(const QString &fileName)
{
	QImage loadedImage;
	if (!loadedImage.load(fileName))
		return false;

	QSize newSize = loadedImage.size();
	resizeImage(&loadedImage, newSize);
	image = loadedImage;
	this->resize(image.size());
	this->setMinimumSize(image.size());
	modified = false;
	update();
	return true;
}

bool PaintWidget::newImage(int x, int y)
{
	QImage loadedImage(x,y,QImage::Format_RGB32);
	loadedImage.fill(qRgb(255, 255, 255));
	QSize newSize = loadedImage.size();
	resizeImage(&loadedImage, newSize);
	image = loadedImage;
	this->resize(image.size());
	this->setMinimumSize(image.size());
	modified = false;
	update();
	return true;
}

bool PaintWidget::saveImage(const QString &fileName)
{
	QImage visibleImage = image;
	resizeImage(&visibleImage, size());

	if (visibleImage.save(fileName,"png")) {
		modified = false;
		return true;
	}
	else {
		return false;
	}
}

void PaintWidget::setPenColor(const QColor &newColor)
{
	myPenColor = newColor;
}

void PaintWidget::setPenWidth(int newWidth)
{
	myPenWidth = newWidth;
}

void PaintWidget::clearImage()
{
	image.fill(qRgb(255, 255, 255));
	modified = true;
	update();
}

void PaintWidget::mousePressEvent(QMouseEvent *event)
{
	//lavy klik mysi (Spir)
	if (event->button() == Qt::LeftButton) {
		lastPoint = event->pos();
		painting = true;
		body.push_back(lastPoint);
	}
	//pravy klik mysi (ja)
	if (event->button() == Qt::RightButton) {
		//odchyti a uchova sa bod kde sa ma posunut objekt
		lastPoint = event->pos();
		bod_posunu = lastPoint;
	}
}

void PaintWidget::mouseDoubleClickEvent(QMouseEvent *event) {

}

void PaintWidget::mouseMoveEvent(QMouseEvent *event)
{
	if ((event->buttons() & Qt::LeftButton) && painting)
		drawLineTo(event->pos());
}

void PaintWidget::mouseReleaseEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton && painting) {
		drawLineTo(event->pos());
		painting = false;
	}
}

void PaintWidget::paintEvent(QPaintEvent *event)
{
	QPainter painter(this);
	QRect dirtyRect = event->rect();
	painter.drawImage(dirtyRect, image, dirtyRect);
}

void PaintWidget::resizeEvent(QResizeEvent *event)
{
	QWidget::resizeEvent(event);
}

void PaintWidget::drawLineTo(const QPoint &endPoint)
{
	QPainter painter(&image);
	painter.setPen(QPen(myPenColor, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	modified = true;
	painter.drawPoint(lastPoint);

	update();
}

void PaintWidget::resizeImage(QImage *image, const QSize &newSize)
{
	if (image->size() == newSize)
		return;

	QImage newImage(newSize, QImage::Format_RGB32);
	newImage.fill(qRgb(255, 255, 255));
	QPainter painter(&newImage);
	painter.drawImage(QPoint(0, 0), *image);
	*image = newImage;
}

void PaintWidget::nastav_farbu_hranici(int red, int green, int blue, int a) {
	printf("Nastavujem farbu hranici... \n");

	farba_hranici.setRgb(red, green, blue, a);
}

void PaintWidget::nastav_farbu_vyplne(int red, int green, int blue, int a) {
	printf("Nastavujem farbu vyplne... \n");

	farba_vyplne.setRgb(red, green, blue, a);
}

void PaintWidget::dda() {
	printf("Vykonavam dda... \n");

	int x1, y1, x2, y2, dx, dy, krok;
	float zvacsene_x, zvacsene_y, x, y;

	QPainter painter(&image);
	painter.setPen(QPen(farba_hranici, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	
	//prejde vsetky body
	for (int i = 0; i < body.size(); i++) {
		//zaradi do premennych body
		if (i != body.size() - 1) {
			x1 = body[i].x();
			y1 = body[i].y();
			x2 = body[i + 1].x();
			y2 = body[i + 1].y();
		}
		else {
			x1 = body[i].x();
			y1 = body[i].y();
			x2 = body[0].x();
			y2 = body[0].y();
		}

		//vypocita rozdiel medzi bodmi
		dx = x2 - x1;
		dy = y2 - y1;

		//pre zaciatok pripadi zaciatocny bod
		x = x1;
		y = y1;

		//rozhodne sa v ktorom smere sa bude krokuvat
		if (abs(dx) > abs(dy)) {
			krok = abs(dx);
		}
		else {
			krok = abs(dy);
		}

		//o kolko sa budu suradnice zvacsovat
		// if dx > dy -> zvacsenie_x = 1
		// if dx < dy -> zvacsenie_y = 1
		zvacsene_x = dx / (float)krok;
		zvacsene_y = dy / (float)krok;

		//zvacsovanie a vykreslovanie "ciare"
		for (int j = 0; j < krok; j++) {
			x += zvacsene_x;
			y += zvacsene_y;
			painter.drawPoint(x, y);
		}
	}
	
	update();
}

//UZ NEPOTREBY
void PaintWidget::bresen() {
	printf("Vykonavam Bresen... \n");

	int x1, y1, x2, y2, dx, dy, dx1, dy1, krok;
	int x, y;
	float chyba;
	int k1, k2, p;

	QPainter painter(&image);
	painter.setPen(QPen(farba_hranici, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));

	//prejde vsetky body
	for (int i = 0; i < body.size(); i++) {
		//zaradi do premennych body
		if (i != body.size() - 1) {
			x1 = body[i].x();
			y1 = body[i].y();
			x2 = body[i + 1].x();
			y2 = body[i + 1].y();
		}
		else {
			x1 = body[i].x();
			y1 = body[i].y();
			x2 = body[0].x();
			y2 = body[0].y();
		}

		//vypocita rozdiel medzi bodmi
		dx = x2 - x1;
		dy = y2 - y1;

		//aby sme kreslili vzdy iba ciaru do 45 stupnov, nie strmsiu
		if (abs(dy) > abs(dx)) {
			std::swap(x1, y1);
			std::swap(x2, y2);
		}

		//aby sme vzdy kreslili ciaru s lava na pravo
		if (x1 > x2) {
			std::swap(x1, x2);
			std::swap(y1, y2);
		}

		//vypocita rozdiel medzi vymenenymi bodmi
		dx1 = x2 - x1;
		dy1 = y2 - y1;

		chyba = dx1 / 2.0f;

		//alebo sa ciara bude kreslit dolu alebo hore vzhladom na y
		if (y1 < y2) {
			krok = 1;
		}
		else {
			krok = -1;
		}

		y = y1;

		//zvacsuje x vzdy o 1
		for (int x = x1; x < x2; x++) {
			//ak sa hore kvoli strmosti ciare zamenili x a y, teraz to vratime spat
			//kreslime y, x a ak nie, tak normalne x, y
			if (abs(dy) > abs(dx)) {
				painter.drawPoint(y, x);
			}
			else {
				painter.drawPoint(x, y);
			}

			chyba -= abs(dy1);
			if (chyba < 0) {
				y += krok;
				chyba += dx1;
			}
		}
	}

	update();
}

void PaintWidget::kruznica_bresen() {
	printf("Kreslim kruznicu Bresenhamovim algoritmom... \n");

	QPainter painter(&image);
	painter.setPen(QPen(farba_hranici, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));

	//stred kruznice bude prvy bod zadany
	int xs = body[0].x();
	int ys = body[0].y();
	//vypocet vzdialenosti 2 bodov
	//d = odmocnica ((x2 - x1)^2 + (y2 - y1)^2)
	int polomer = (int)(abs((sqrt(((body[1].x() - body[0].x()) * (body[1].x() - body[0].x())) + ((body[1].y() - body[0].y()) * (body[1].y() - body[0].y()))) + 0.5)));

	int p1 = 1 - polomer;
	int x = 0;
	int y = polomer;
	int dva_x = 3;
	int dva_y = (2 * polomer) - 2;

	while (x <= y) {
		painter.drawPoint(x + xs, y + ys);
		painter.drawPoint(y + xs, x + ys);
		painter.drawPoint(y + xs, -x + ys);
		painter.drawPoint(x + xs, -y + ys);
		painter.drawPoint(-x + xs, -y + ys);
		painter.drawPoint(-y + xs, -x + ys);
		painter.drawPoint(-y + xs, x + ys);
		painter.drawPoint(-x + xs, y + ys);

		if (p1 > 0) {
			p1 = p1 - dva_y;
			dva_y -= 2;
			y--;
		}

		p1 += dva_x;
		x++;
		dva_x += 2;
	}

	update();
}

void PaintWidget::vyplnenie_kruznica() {
	printf("Vykonavam vyplnenie kruznici... \n");

	QPainter painter(&image);
	painter.setPen(QPen(farba_vyplne, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));

	//stred kruznice bude prvy bod zadany
	int xs = body[0].x();
	int ys = body[0].y();
	//vypocet vzdialenosti 2 bodov
	//d = odmocnica ((x2 - x1)^2 + (y2 - y1)^2)
	int polomer = (int)(abs((sqrt(((body[1].x() - body[0].x()) * (body[1].x() - body[0].x())) + ((body[1].y() - body[0].y()) * (body[1].y() - body[0].y()))) + 0.5)));

	//prejde po sirky
	for (int i = 0; i < polomer * 2; i++) {
		//prejde po vysky
		for (int j = 0; j < polomer * 2; j++) {
			//pocita akoze stvorec co je okolo kruhu
			int dx = polomer - i;
			int dy = polomer - j;

			//vyplna iba vnutro kruhu, cize oblast mensiu ako poloer^2
			if ((dx * dx + dy * dy) <= (polomer * polomer)) {
				painter.drawPoint(xs + dx, ys + dy);
			}
		}
	}

	update();
}

void PaintWidget::scan_line() {
	printf("Vykonavam scan-line... \n");

	int x1, x2, y1, y2;
	int dx, dy;

	QPainter painter(&image);
	painter.setPen(QPen(farba_vyplne, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));

	//struktura pre hrany, ktora obsahuje zaciatocny bod (x, y)
	//koncovi bod y, obratenu smernicu w a operator porovnania
	struct hrana {
		int x1;
		int y1;
		int y2;
		float w;
		bool operator< (const hrana &h) const {
			if (y1 != h.y1) {
				return y1 < h.y1;
			}

			if (y1 == h.y1) {
				if (x1 == h.x1) {
					return w < h.w;
				}
				else {
					return x1 < h.x1;
				}
			}
		}
	};

	//struktura aktivnich hran, obsahuje index aktivnej hrany
	//aktualne x a operator porovnania
	struct aktivna_hrana {
		int hrana_index;
		float xa;
		bool operator< (const aktivna_hrana &ah) const {
			return xa < ah.xa;
		}
	};

	int ymin = 0, ymax = 0;
	std::vector<hrana> hrany;
	hrana tmp;

	//prejde vsetky body
	for (int i = 0; i < body.size(); i++) {
		//zaradi do premennych body
		if (i != body.size() - 1) {
			x1 = body[i].x();
			y1 = body[i].y();
			x2 = body[i + 1].x();
			y2 = body[i + 1].y();
		}
		else {
			x1 = body[i].x();
			y1 = body[i].y();
			x2 = body[0].x();
			y2 = body[0].y();
		}

		//vodorovne hrany sa vyhodia
		if ((y2 - y1) != 0) {
			//aby vzdy bola z hora nadol hrana
			if (y1 > y2) {
				std::swap(x1, x2);
				std::swap(y1, y2);
			}

			//skratime hranicu o jeden pixel v y-osi
			y2 -= 1;

			//vypocita rozdiel medzi bodmi
			dx = x2 - x1;
			dy = y2 - y1;

			//vlozime hranu do tabulky hran
			tmp.x1 = x1;
			tmp.y1 = y1;
			tmp.y2 = y2;
			tmp.w = dx / (float)dy;

			hrany.push_back(tmp);

			//nastav ymax
			if (tmp.y2 > ymax) {
				ymax = tmp.y2;
			}
		}
	}

	//vysortujem tabulku hran
	std::sort(hrany.begin(), hrany.end());

	ymin = hrany[0].y1;
	std::vector<aktivna_hrana> aktivne_hrany;
	aktivna_hrana aktivna;

	//ScanLine
	for (int ya = ymin; ya <= ymax; ya++) {
		for (int i = 0; i < hrany.size(); i++) {
			if (ya == hrany[i].y1) {
				aktivna.hrana_index = i;
				aktivna.xa = hrany[i].w * ya - hrany[i].w * hrany[i].y1 + hrany[i].x1;

				aktivne_hrany.push_back(aktivna);
			}
		}

		std::sort(aktivne_hrany.begin(), aktivne_hrany.end());

		//vykreslovanie medzi
		for (int i = 0; i < aktivne_hrany.size() - 1; i++) {
			if ((int)(aktivne_hrany[i].xa + 0.5) != (int)(aktivne_hrany[i + 1].xa + 0.5)) {
				QPoint A((int)(aktivne_hrany[i].xa + 1.5), ya);
				QPoint B((int)(aktivne_hrany[i + 1].xa - 0.5), ya);
	
				for (int x = A.x(); x <= B.x(); x++) {
					painter.drawPoint(x, ya);
				}
			}

			aktivne_hrany[i].xa += hrany[aktivne_hrany[i].hrana_index].w;
			aktivne_hrany[i + 1].xa += hrany[aktivne_hrany[i + 1].hrana_index].w;

			if (i + 2 >= aktivne_hrany.size()) {
				break;
			}
			i++;
		}

		for (int i = 0; i < aktivne_hrany.size(); i++) {
			if (ya == hrany[aktivne_hrany[i].hrana_index].y2) {
				aktivne_hrany.erase(aktivne_hrany.begin() + i);
				i--;
			}
		}
	}

	update();
}

void PaintWidget::posun() {
	printf("Vykonavam posun... \n");

	int px, py, x, y;

	//vypocita posun
	px = bod_posunu.x() - body[0].x();
	py = bod_posunu.y() - body[0].y();

	//vypocita nove hodnoty
	for (int i = 0; i < body.size(); i++) {
		x = (int)(body[i].x() + px);
		y = (int)(body[i].y() + py);
		body[i].setX(x);
		body[i].setY(y);
	}
}

void PaintWidget::otoc(float hodnota, bool smer) {
	printf("Vykonavam otocenie... \n");

	int sx, sy, x = 0, y = 0;
	float hodnota_rad = 0;

	//prevedie stupne do riadianov
	hodnota_rad = hodnota * (M_PI / 180);

	//bod okolo ktoreho ideme otacat
	sx = body[0].x();
	sy = body[0].y();

	//proti smeru hodinovych ruciciek
	if (!smer) {
		for (int i = 0; i < body.size(); i++) {
			x = (int)((body[i].x() - sx) * cos(hodnota_rad) + (body[i].y() - sy) * sin(hodnota_rad) + sx);
			y = (int)(-(body[i].x() - sx) * sin(hodnota_rad) + (body[i].y() - sy) * cos(hodnota_rad) + sy);
			body[i].setX(x);
			body[i].setY(y);
		}
	}
	//v smere hodinovych ruciciek
	else {
		for (int i = 0; i < body.size(); i++) {
			x = (int)((body[i].x() - sx) * cos(hodnota_rad) - (body[i].y() - sy) * sin(hodnota_rad) + sx);
			y = (int)((body[i].x() - sx) * sin(hodnota_rad) + (body[i].y() - sy) * cos(hodnota_rad) + sy);
			body[i].setX(x);
			body[i].setY(y);
		}
	}
}

void PaintWidget::skaluj(float hodnota) {
	printf("Vykonavam skalovanie... \n");

	int sx, sy, x, y;

	//stred moj
	sx = body[0].x();
	sy = body[0].y();

	//vypocita nove hodnoty
	for (int i = 0; i < body.size(); i++) {
		//vypocitame vzdialenost od stredu
		x = body[i].x() - sx;
		y = body[i].y() - sy;

		//preskalujeme
		x = (int)(hodnota * x);
		y = (int)(hodnota * y);
		body[i].setX(x + sx);
		body[i].setY(y + sy);
	}
}

void PaintWidget::preklop_ciara() {
	QPainter painter(&image);
	painter.setPen(QPen(farba_hranici, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));

	//spravy ciaru
	painter.drawLine(body[0].x(), 0, body[0].x(), image.height());
}

void PaintWidget::preklop() {
	printf("Vykonavam preklopenie... \n");

	int a, b, c, x, y;
	float d;

	//vypovet normaly
	//a = b1.y - b2.y
	//b = b2.x - b1.x
	a = body[0].y() - (body[0].y() - 1);
	b = body[0].x() - body[0].x();

	//dopocitame c
	c = -(a * body[0].x()) - (b * body[0].y());

	//vypocita nove hodnoty
	for (int i = 0; i < body.size(); i++) {
		//vypocet d
		d = ((a * body[i].x()) + (b * body[i].y()) + c) / ((a * a) + (b * b));

		x = (int)(body[i].x() - 2 * a * d);
		y = (int)(body[i].y() - 2 * b * d);
		body[i].setX(x);
		body[i].setY(y);
	}
}

void PaintWidget::skos(float hodnota, bool smer) {
	printf("Vykonavam skosenie... \n");

	int sx, sy, x, y;

	//stred moj
	sx = body[0].x();
	sy = body[0].y();

	//v smere osi y
	if (smer) {
		for (int i = 0; i < body.size(); i++) {
			//vypocitame vzdialenost od stredu
			x = body[i].x() - sx;
			y = body[i].y() - sy;

			x = (int)(x - hodnota * y);
			body[i].setX(x + sx);
		}
	}
	//v smere osi x
	else {
		for (int i = 0; i < body.size(); i++) {
			//vypocitame vzdialenost od stredu
			x = body[i].x() - sx;
			y = body[i].y() - sy;

			y = (int)(y - hodnota * x);
			body[i].setY(y + sy);
		}
	}
}

void PaintWidget::vymaz_vsetko() {
	printf("Vymazavam vsetko... \n");

	//ocisti vsetky vektory
	clearImage();
	body.clear();
	bod_posunu.setX(body[0].x());
	bod_posunu.setY(body[0].y());

	posunutie = false;
}

void PaintWidget::bezierova_krivka() {
	printf("Kreslim Bezierovu krivku... \n");

	//vzdy bude 1000
	int pocet_deleni = 1000;

	int n = body.size();
	float t = 0;
	QPointF prvy, druhy;
	std::vector<QPointF> P1;
	std::vector<QPointF> P2;

	//ak je zadany iba jeden bod
	if (n < 2) {
		QMessageBox box;
		box.setText("Musite zadat aspon 2 body.");
		box.exec();

		vymaz_vsetko();

		return;
	}

	//nastavenie velkosti 
	P1.resize(n);
	P2.resize(n);

	//priradi k P1 povodbe body
	for (int k = 0; k < n; k++) {
		P1[k] = (QPointF)body[k];
	}

	//prvy bod pre dda
	prvy = P1[0];

	//prejde celu krivku podelenu na pocet_deleni casti
	for (int delenie = 0; delenie <= pocet_deleni; delenie++) {
		//vypocet krociku
		t = delenie * (1.0 / pocet_deleni);
		
		//to je ten vypocet
		for (int j = 1; j < n; j++) {
			for (int i = 0; i < n - j; i++) {
				P2[i] = (1 - t) * P1[i] + t *  P1[i + 1];
			}
			P1 = P2;
		}

		//nastavy druhy bod, vykona dda a zas nastavy prvy bod
		druhy = P1[0];
		dda_point(prvy.toPoint(), druhy.toPoint());
		prvy = P1[0];

		//priradi k P1 povodbe body
		for (int k = 0; k < n; k++) {
			P1[k] = (QPointF)body[k];
		}
	}

	//vymaze vektory
	P1.clear();
	P2.clear();
}

void PaintWidget::coonsova_kubika() {
	printf("Kreslim Coonsovu kubiku b-spline... \n");

	int n = body.size();
	QPoint prvy, druhy;
	//vzdy bude 1000
	int pocet_deleni = 1000;
	float t = 0;

	//ak sa zadanie 4 body a menej
	if (n < 5) {
		QMessageBox box;
		box.setText("Musite zadat aspon 5 bodov.");
		box.exec();

		vymaz_vsetko();

		return;
	}

	//prejde vsetky body do 4 od konca
	for (int i = 0; i < body.size() - 4; i++) {
		//prejde celu krivku podelenu na pocet_deleni casti
		for (int delenie = 0; delenie <= pocet_deleni; delenie++) {
			//vypocet krociku
			t = delenie * (1.0 / pocet_deleni);

			//nastavy prvy bod
				prvy = druhy;

			//vypocet koeficientov
			float koeficient0 = vypocet_koeficientov0(t);
			float koeficient1 = vypocet_koeficientov1(t);
			float koeficient2 = vypocet_koeficientov2(t);
			float koeficient3 = vypocet_koeficientov3(t);

			//nastavy druhy bod
			druhy.setX((body[i].x() * koeficient0) + (body[i + 1].x() * koeficient1) + (body[i + 2].x() * koeficient2) + (body[i + 3].x() * koeficient3));
			druhy.setY((body[i].y() * koeficient0) + (body[i + 1].y() * koeficient1) + (body[i + 2].y() * koeficient2) + (body[i + 3].y() * koeficient3));

			//ked sme na zaciatku a druhy bod este nebov nastaveny
			if (t == 0 && i == 0) { 
				prvy = druhy;
			}

			//vykona dda
			dda_point(prvy, druhy);
		}
	}
}

void PaintWidget::dda_point(QPoint prvy, QPoint druhy) {
	//vykresli na obrazovku
	int x1, y1, x2, y2, dx, dy, krok;
	float zvacsene_x, zvacsene_y, x, y;

	QPainter painter(&image);
	painter.setPen(QPen(myPenColor, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));

	x1 = prvy.x();
	y1 = prvy.y();
	x2 = druhy.x();
	y2 = druhy.y();

	//vypocita rozdiel medzi bodmi
	dx = x2 - x1;
	dy = y2 - y1;

	//pre zaciatok pripadi zaciatocny bod
	x = x1;
	y = y1;

	//rozhodne sa v ktorom smere sa bude krokuvat
	if (abs(dx) > abs(dy)) {
		krok = abs(dx);
	}
	else {
		krok = abs(dy);
	}

	//o kolko sa budu suradnice zvacsovat
	// if dx > dy -> zvacsenie_x = 1
	// if dx < dy -> zvacsenie_y = 1
	zvacsene_x = dx / (float)krok;
	zvacsene_y = dy / (float)krok;

	//zvacsovanie a vykreslovanie "ciare"
	for (int j = 0; j < krok; j++) {
		x += zvacsene_x;
		y += zvacsene_y;
		painter.drawPoint(x, y);
	}

	update();
}

float PaintWidget::vypocet_koeficientov0(float t) {
	float koeficient = 0;

	koeficient = -((t * t * t) / 6.0) + ((t * t) / 2.0) - (t / 2.0) + (1 / 6.0);

	return koeficient;
}

float PaintWidget::vypocet_koeficientov1(float t) {
	float koeficient = 0;

	koeficient = ((t * t * t) / 2.0) - (t * t) + (2 / 3.0);

	return koeficient;
}

float PaintWidget::vypocet_koeficientov2(float t) {
	float koeficient = 0;

	koeficient = -((t * t * t) / 2.0) + ((t * t) / 2.0) + (t / 2.0) + (1 / 6.0);

	return koeficient;
}

float PaintWidget::vypocet_koeficientov3(float t) {
	float koeficient = 0;

	koeficient = (t * t * t) / 6.0;

	return koeficient;
}

