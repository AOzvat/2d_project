#include "mypainter.h"

MyPainter::MyPainter(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	ui.scrollArea->setWidget(&this->paintWidget);
	ui.scrollArea->setBackgroundRole(QPalette::Dark);
	paintWidget.newImage(800, 600);
}

MyPainter::~MyPainter()
{

}

void MyPainter::ActionOpen()
{
	QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"), "", "image files (*.png *.jpg *.bmp)");
	if (!fileName.isEmpty())
		paintWidget.openImage(fileName);
}

void MyPainter::ActionSave()
{
	QString fileName = QFileDialog::getSaveFileName(this, tr("Save As"), "untitled.png", tr("png Files (*.png)"));
	if (fileName.isEmpty()) {
		return;
	}
	else {
		paintWidget.saveImage(fileName);
	}
}

void MyPainter::EffectClear()
{
	paintWidget.vymaz_vsetko();
}

void MyPainter::ActionNew()
{
	paintWidget.newImage(800, 600);
}

void MyPainter::Help() {
	//sprava pre zaciatok
	QString navod_text;

	//otvorenie suboru
	QFile navod_file("Navod.txt");
	if (navod_file.open(QFile::ReadOnly)) {
		QTextStream navod(&navod_file);
		while (!navod.atEnd()) {
			navod_text = navod_text + "\n" + navod.readLine();
		}
		navod_file.close();
	}

	QMessageBox box;
	box.setText(navod_text);
	box.exec();
}

void MyPainter::KresliClicked()
{
	float hodnota = 0.0;

	//vymaze kresliacu plochu
	paintWidget.clearImage();

	//nastavi zadanu farbu hranici
	nastavi_farbu_hranici();

	//nastavi zadanu farbu vyplne
	nastavi_farbu_vyplne();

	//ak sa zaskrtnutie transformacie
	if (ui.checkBox->isChecked()) {
		//posuvanie
		if (ui.comboBox->currentIndex() == 0) {
			paintWidget.posun();
		}

		//otacanie
		if (ui.comboBox->currentIndex() == 1) {
			//nacita hodnotu uhla
			if (ui.lineEdit->text() == "") {
				hodnota = 30.0;
			}
			else {
				hodnota = ui.lineEdit->text().toFloat();
			}

			//v smere hodinovych rucuciek
			if (ui.radioButton->isChecked()) {
				paintWidget.otoc(hodnota, true);
			}

			//proti smeru hodinovych ruciciek
			if (ui.radioButton_2->isChecked()) {
				paintWidget.otoc(hodnota, false);
			}
		}

		//skalovanie
		if (ui.comboBox->currentIndex() == 2) {
			//nacita hodnotu koeficienta
			if (ui.lineEdit->text() == "") {
				hodnota = 2.0;
			}
			else {
				hodnota = ui.lineEdit->text().toFloat();
			}

			paintWidget.skaluj(hodnota);
		}

		//preklopenie
		if (ui.comboBox->currentIndex() == 3) {
			paintWidget.preklop_ciara();
			paintWidget.preklop();
		}

		//skosenie
		if (ui.comboBox->currentIndex() == 4) {
			//nacita hodnotu koeficienta
			if (ui.lineEdit->text() == "") {
				hodnota = 1.0;
			}
			else {
				hodnota = ui.lineEdit->text().toFloat();
			}

			//v smere osi x
			if (ui.radioButton_3->isChecked()) {
				paintWidget.skos(hodnota, true);
			}

			//v smere osi y
			if (ui.radioButton_4->isChecked()) {
				paintWidget.skos(hodnota, false);
			}
		}
	}

	//kruznica alebo polygon alebo Bezierova krivka alebo Coonsova kubika
	//polygon
	if (ui.comboBox_2->currentIndex() == 0) {
		//vykresli objekt
		paintWidget.dda();

		//ak je zaskrtnuty Scan-line
		if (ui.checkBox_2->isChecked()) {
			//vyplni oblast
			paintWidget.scan_line();
		}
	}

	//kruznica
	if (ui.comboBox_2->currentIndex() == 1) {
		paintWidget.kruznica_bresen();

		//ak je zaskrtnuty Scan-line
		if (ui.checkBox_2->isChecked()) {
			//vyplni oblast
			paintWidget.vyplnenie_kruznica();
		}
	}

	//Bezierova krivka
	if (ui.comboBox_2->currentIndex() == 2) {
		paintWidget.bezierova_krivka();
	}

	//Coonsova kubika
	if (ui.comboBox_2->currentIndex() == 3) {
		paintWidget.coonsova_kubika();
	}

}

void MyPainter::nastavi_farbu_hranici() {
	if (ui.lineEdit_3->text() == "" && ui.lineEdit_4->text() == "" && ui.lineEdit_5->text() == "") {
		paintWidget.nastav_farbu_hranici();
	}
	else {
		paintWidget.nastav_farbu_hranici(ui.lineEdit_3->text().toInt(), ui.lineEdit_4->text().toInt(), ui.lineEdit_5->text().toInt());
	}
}

void MyPainter::nastavi_farbu_vyplne() {
	if (ui.lineEdit_7->text() == "" && ui.lineEdit_2->text() == "" && ui.lineEdit_6->text() == "") {
		paintWidget.nastav_farbu_vyplne();
	}
	else {
		paintWidget.nastav_farbu_vyplne(ui.lineEdit_7->text().toInt(), ui.lineEdit_2->text().toInt(), ui.lineEdit_6->text().toInt());
	}
}